/**
 * 
 */
package com.nandhi.service;

import java.util.List;

import com.nandhi.centconversion.CentDenomination;
import com.nandhi.centconversion.CountryDenominationService;
import com.nandhi.centconversion.ICentDenomination;
import com.nandhi.centconversion.ICountryDenominationService;

/**
 * @author Nandhi
 *
 */
public class ConvertCentService implements IConvertCentService {

	ICountryDenominationService denominationService = CountryDenominationService.getInstance();
	ICentDenomination centDomination = CentDenomination.getInstance();

	private static ConvertCentService centService;

	private ConvertCentService() {

	}

	public static ConvertCentService getInstance() {
		if (null == centService) {
			centService = new ConvertCentService();
		}
		return centService;
	}

	public void getDenomination4Currency(String country, int cents) {

		// check if the country exists in the list
		if (null != getCountryDenomination(country)) {
			centDomination.denominationCalc(cents, getCountryDenomination(country));
		} else {
			System.out.println("Country is not in the list");
		}

	}

	@Override
	public void addCountryDenomination(String country, List<Integer> denominationArray) {
		denominationService.addCountryDenomination(country, denominationArray);

	}

	@Override
	public List<Integer> getCountryDenomination(String country) {
		return denominationService.getCountryDenomination(country);
	}

}
