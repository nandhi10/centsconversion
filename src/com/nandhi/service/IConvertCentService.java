/**
 * 
 */
package com.nandhi.service;

import java.util.List;

/**
 * @author Nandhi
 *
 */
public interface IConvertCentService {
	
	public void getDenomination4Currency(String country, int cents);
	
	public void addCountryDenomination(String country, List<Integer> denominationArray);
	
	public List<Integer> getCountryDenomination(String country);

}
