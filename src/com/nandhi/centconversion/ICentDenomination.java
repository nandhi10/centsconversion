/**
 * 
 */
package com.nandhi.centconversion;

import java.util.List;
import java.util.Map;

/**
 * @author Nandhi
 *
 */
public interface ICentDenomination {
	
	public Map<Integer, Integer> denominationCalc(int cents, List<Integer> denominatorArray);

}
