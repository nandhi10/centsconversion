/**
 * 
 */
package com.nandhi.centconversion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Nandhi
 *
 */
public class CentDenomination implements ICentDenomination {

	private static CentDenomination centDenomination;

	private CentDenomination() {
		System.out.println("CentDenomination");
	}

	public static CentDenomination getInstance() {
		if (null == centDenomination) {
			centDenomination = new CentDenomination();
		}
		return centDenomination;
	}

	/**
	 * 
	 * @param cents
	 * @param denominator
	 */
	private int getCentsLeft(int cents, int denominator) {
		System.out.println("Cent Conversion getCentsLeft " + cents % denominator);
		return cents % denominator;
	}

	private int getDenominationCount(int cents, int denominator) {
		System.out.println("Cent Conversion getDenominationCount " + cents / denominator);
		return cents / denominator;
	}

	/**
	 * 
	 * @param cents
	 * @param denominatorArray
	 */
	public Map<Integer, Integer> denominationCalc(int cents, List<Integer> denominatorArray) {
		Map<Integer, Integer> denominatorMap = new HashMap<>();

		for (Integer denominator : denominatorArray) {
			int denominatorCnt = getDenominationCount(cents, denominator.intValue());
			denominatorMap.put(denominator, denominatorCnt);
			cents = getCentsLeft(cents, denominator.intValue());
		}

		System.out.println("Sorted Output" + denominatorMap);
		return denominatorMap.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

}
