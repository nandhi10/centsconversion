/**
 * 
 */
package com.nandhi.centconversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Nandhi
 *
 */
public class CountryDenominationService implements ICountryDenominationService {

	static Map<String, ArrayList<Integer>> countryWiseDenomination = new HashMap<>();

	private static CountryDenominationService denominationServ;

	int[] dollarDenoArr = { 100, 50, 25, 10, 5, 1 };
	int[] euroDenoArr = { 100, 50, 20, 10, 5, 1 };

	public void addCountryDenomination(String country, List<Integer> denominationArray) {
		countryWiseDenomination.put(country, (ArrayList<Integer>) denominationArray);
	}

	public List<Integer> getCountryDenomination(String country) {
		return countryWiseDenomination.get(country);
	}

	private CountryDenominationService() {
		// singleton
	}

	/**
	 * return singleton object;
	 * 
	 * @return
	 */
	public static CountryDenominationService getInstance() {
		if (null == denominationServ) {
			denominationServ = new CountryDenominationService();
			denominationServ.addDefaultDenomination();
		}
		return denominationServ;
	}

	/**
	 * add a default Denomination
	 */
	private void addDefaultDenomination() {

		denominationServ.addCountryDenomination("USA",
				Arrays.stream(dollarDenoArr).boxed().collect(Collectors.toList()));

		denominationServ.addCountryDenomination("EURO",
				Arrays.stream(euroDenoArr).boxed().collect(Collectors.toList()));

	}

}
