/**
 * 
 */
package com.nandhi.centconversion;

import java.util.List;

/**
 * @author Nandhi
 *
 */
public interface ICountryDenominationService {
	
	public void addCountryDenomination(String country, List<Integer> denominationArray);
	
	public List<Integer> getCountryDenomination(String country);

}
