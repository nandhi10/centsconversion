package com.nandhi;

import com.nandhi.service.ConvertCentService;

/**
 * 
 */

/**
 * @author Nandhi
 *
 */
public class CentConversion {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CentConversion centConversion = new CentConversion();
		centConversion.triggerCentConversion();

	}

	public void triggerCentConversion() {
		ConvertCentService convertCentService = ConvertCentService.getInstance();
		convertCentService.getDenomination4Currency("USA", 28813);
		convertCentService.getDenomination4Currency("EURO", 14444);
	}

}
